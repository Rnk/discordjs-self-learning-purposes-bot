const botconfig = require("./config.json");
const Discord = require("discord.js");
const fs = require("fs");
const client = new Discord.Client({disableEveryone: true});
client.commands = new Discord.Collection();

fs.readdir("./commands/", (err, files) => {

    if(err) console.log(err);
    let jsfile = files.filter(f => f.split(".").pop() === "js");
    if(jsfile.length <= 0){
        console.log("err commands not found");
        return;
    }

    jsfile.forEach((f, i) =>{
        let props = require(`./commands/${f}`);
        console.log(`${f} successfully loaded`);
        client.commands.set(props.help.name, props);
    });
});

client.on("ready", async () => {
    console.log(`${client.user.username} is online on ${client.guilds.size} servers!`);
    //client.user.setActivity("text", {type: "WATCHING"});

});

client.on("message", async message => {
    if (!message.content.startsWith(botconfig.prefix)) return;
    if (message.author.bot) return;
    if (message.channel.type === "dm") return;
    let prefix = botconfig.prefix;
    let messageArray = message.content.split(" ");
    let cmd = messageArray[0];
    let args = messageArray.slice(1);
    let commandfile = client.commands.get(cmd.slice(prefix.length));
    if(commandfile) commandfile.run(client,message,args);
});

client.login(botconfig.token);